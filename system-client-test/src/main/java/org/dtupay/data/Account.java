/*
 * Author: Eriks Markevics(s202741)
 */

package org.dtupay.data;

import org.dtupay.data.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Account {
    public String dtuPayId;
    public String bankAccountId;
    public User user;
}
