/*
 * Author: Eriks Markevics(s202741)
 */

package org.dtupay.api;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.dtupay.data.AccountInfo;
import org.dtupay.data.Report;


public class ManagerAPI {
    private static WebTarget baseUrl = null;

    public static void setup() {
        if (baseUrl == null) {
            Client client = ClientBuilder.newClient();
            baseUrl = client.target("http://localhost:8081/");
//            baseUrl = client.target("http://fm-07.compute.dtu.dk:8081/");
        }
    }

    public static Report getReport() {
        setup();
        Response response = baseUrl.path("managers/report").request().accept(MediaType.APPLICATION_JSON).get();
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            return response.readEntity(Report.class);
        } else {
            return null;
        }
    }

    public static boolean removeAccount(String dtuPayId) {
        setup();
        Response response = baseUrl.path("managers/remove_account/" + dtuPayId).request().accept(MediaType.APPLICATION_JSON).get();
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            return true;
        } else {
            return false;
        }
    }
}

