/*
 * Author: Eriks Markevics(s202741)
 */

package org.dtupay.api;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.dtupay.data.*;
import org.dtupay.utils.ResponseCode;

public class MerchantAPI {

	private static WebTarget baseUrl;

	public static void setup() {
		if (baseUrl == null) {
			Client client = ClientBuilder.newClient();
			baseUrl = client.target("http://localhost:8081/");
//			baseUrl = client.target("http://fm-07.compute.dtu.dk:8081/");
		}
	}

	public static Report getReport(String customerDTUPayId) {
		setup();
		Response response = baseUrl.path("merchants/" + customerDTUPayId + "/report").request().accept(MediaType.APPLICATION_JSON)
				.get();
		if (response.getStatus() == Response.Status.OK.getStatusCode()) {
			return response.readEntity(Report.class);
		}
		else
		{
			return null;
		}
	}

	public static ResponseType registerAccount(String cpr, String firstName, String lastName, String bankAccountId) throws Exception {
		setup();
		User user = new User(cpr, firstName, lastName);
		Response response = baseUrl.path("merchants/registration").request().accept(MediaType.APPLICATION_JSON)
				.post(Entity.json(new CreateAccountData(user, bankAccountId)));
		return response.readEntity(ResponseType.class);
	}

	public static ResponseType pay(PaymentRequest paymentInfo) throws Exception {
		setup();
		Response response = baseUrl.path("merchants/payment").request().accept(MediaType.APPLICATION_JSON)
				.post(Entity.json(paymentInfo));
		return response.readEntity(ResponseType.class);
	}

}
