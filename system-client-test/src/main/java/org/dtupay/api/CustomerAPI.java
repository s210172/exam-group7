/*
 * Author: Eriks Markevics(s202741)
 */

package org.dtupay.api;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.dtupay.data.AccountInfo;
import org.dtupay.data.CreateAccountData;
import org.dtupay.data.Report;
import org.dtupay.data.RequestTokens;
import org.dtupay.data.ResponseType;
import org.dtupay.data.Tokens;
import org.dtupay.data.User;
import org.dtupay.exceptions.InvalidTokenNumException;


public class CustomerAPI {
	private static WebTarget baseUrl;
	
	public static void setup() {
		if (baseUrl == null) {
			Client client = ClientBuilder.newClient();
			baseUrl = client.target("http://localhost:8081/");
//			baseUrl = client.target("http://fm-07.compute.dtu.dk:8081/");
		}
	}

	public static Report getReport(String customerDTUPayId) {
		setup();
		Response response = baseUrl.path("customers/" + customerDTUPayId + "/report").request().accept(MediaType.APPLICATION_JSON)
				.get();
		if (response.getStatus() == Response.Status.OK.getStatusCode()) {
			return response.readEntity(Report.class);
		}
		else {
			return null;
		}
	}

	public static Tokens requestNewTokens(String customerDTUPayId, int num) throws InvalidTokenNumException {
		setup();
		Response response = baseUrl.path("customers/" + customerDTUPayId + "/tokens/" + Integer.toString(num)).request().accept(MediaType.APPLICATION_JSON)
				.get();
		System.out.println("[wenjie]:request token with merchant ID:"+ customerDTUPayId);
		if (response.getStatus() == Response.Status.OK.getStatusCode()) {
			return response.readEntity(Tokens.class);
		}
		else {
			ResponseType r = response.readEntity(ResponseType.class);
			System.out.println("[wenjie]:request new tokens failed:"+r.message);
			return null;
		}
	}

	public static ResponseType registerAccount(String cpr, String firstName, String lastName, String bankAccountId)  {
		setup();
		User user = new User(cpr, firstName, lastName);
		Response response = baseUrl.path("customers/registration").request().accept(MediaType.APPLICATION_JSON)
				.post(Entity.json(new CreateAccountData(user, bankAccountId)));
        return response.readEntity(ResponseType.class);
	}
}
