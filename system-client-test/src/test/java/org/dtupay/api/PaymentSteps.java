/*
 * Author: Carlo Meroni (s212969)
 */


package org.dtupay.api;

import java.math.BigDecimal;
import java.util.Objects;

import org.dtupay.data.PaymentRequest;
import org.dtupay.data.Report;
import org.dtupay.data.ResponseType;
import org.dtupay.data.Tokens;
import org.dtupay.exceptions.InvalidTokenNumException;

import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.User;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.dtupay.utils.BankSOAPService;
import org.dtupay.utils.ResponseCode;

import static org.junit.Assert.*;

public class PaymentSteps {

    User customer;
    String customerDTUPayID = null;
    String customerBankID = null;
    Tokens customerTokens = null;

    User merchant = new User();
    String merchantBankID = null;
    String merchantDTUPayID = null;

    Boolean donePayment = true;
    ResponseType responseType;
    Report report = null;
    boolean reportForMerchant = false;


    @Before
    public void testBefore() {
        BankSOAPService.removeAllAccounts();
    }

    @Given("the customer {string} {string} with CPR {string} has a bank account with balance {int}")
    public void the_customer_with_cpr_has_a_bank_account_with_balance(String firstName, String lastName, String cpr,
                                                                      Integer amount) {
        customer = new User();
        customer.setCprNumber(cpr);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customerBankID = BankSOAPService.createAccount(customer, BigDecimal.valueOf(amount));
        System.out.println("Customer BankAccountID " + customerBankID);
        customerTokens = new Tokens();
    }

    @Given("that the customer is registered with DTU Pay")
    public void that_the_customer_is_registered_with_dtu_pay() {
        try {
            ResponseType response = CustomerAPI.registerAccount(customer.getCprNumber(), customer.getFirstName(), customer.getLastName(), customerBankID);
            if (Objects.equals(response.code, ResponseCode.REGISTRATION_SUCCESSFUL.getCode())) {
                customerDTUPayID = response.message;
            }
            assertEquals(ResponseCode.REGISTRATION_SUCCESSFUL.getCode(), response.code);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Given("the customer requested for his tokens")
    public void theCustomerRequestedForHisTokens() throws InvalidTokenNumException {
        customerTokens = CustomerAPI.requestNewTokens(customerDTUPayID, 5);
        assertNotNull(customerTokens);
        assertTrue(customerTokens.tokens.size() > 0);
    }

    @Given("a merchant {string} {string} with CPR {string} has a bank account with balance {int}")
    public void a_merchant_with_cpr_has_a_bank_account_with_balance(String firstName, String lastName, String cpr,
                                                                    Integer amount) {
        merchant = new User();
        merchant.setCprNumber(cpr);
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchantBankID = BankSOAPService.createAccount(merchant, BigDecimal.valueOf(amount));
        System.out.println("Merchant BankAccountID " + merchantBankID);
    }

    @Given("that the merchant is registered with DTU Pay")
    public void that_the_merchant_is_registered_with_dtu_pay() {
        try {
            ResponseType response = MerchantAPI.registerAccount(merchant.getCprNumber(), merchant.getFirstName(), merchant.getLastName(), merchantBankID);
            if (Objects.equals(response.code, ResponseCode.REGISTRATION_SUCCESSFUL.getCode())) {
                merchantDTUPayID = response.message;

            }
            System.out.println("response.code == 0" 
            + response.code);
            assertEquals(ResponseCode.REGISTRATION_SUCCESSFUL.getCode(), response.code);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @When("the merchant initiates a payment for {int} kr by the customer")
    public void the_merchant_initiates_a_payment_for_kr_by_the_customer(Integer amount) throws Exception {
        String firstToken = customerTokens.tokens.remove(0);
        PaymentRequest payment = new PaymentRequest(amount, firstToken, merchantDTUPayID, "description");

        responseType = MerchantAPI.pay(payment);
        if (Objects.equals(responseType.code, ResponseCode.PAYMENT_SUCCESSFUL.getCode())) {
            donePayment = true;
        } else {
            System.out.println("Payment failed with message: " + responseType.message);
            donePayment = false;
        }
    }

    @Then("the payment is successful")
    public void the_payment_is_successful() {
        assertTrue(donePayment);
    }

    @Then("the balance of the customer at the bank is {int} kr")
    public void the_balance_of_the_customer_at_the_bank_is_kr(Integer balance) throws BankServiceException_Exception {
        BigDecimal customerBalance = BankSOAPService.bank.getAccount(customerBankID).getBalance();
        assertEquals(BigDecimal.valueOf(balance), customerBalance);
    }

    @Then("the balance of the merchant at the bank is {int} kr")
    public void the_balance_of_the_merchant_at_the_bank_is_kr(Integer balance) throws BankServiceException_Exception {
        BigDecimal merchantBalance = BankSOAPService.bank.getAccount(merchantBankID).getBalance();
        assertEquals(BigDecimal.valueOf(balance), merchantBalance);
    }

    @When("customer asks for reports")
    public void customerAsksForReports() {
        report = CustomerAPI.getReport(customerDTUPayID);
        reportForMerchant = false;
    }

    @When("merchant asks for reports")
    public void merchantAsksForReports() {
        report = MerchantAPI.getReport(merchantDTUPayID);
        reportForMerchant = true;
    }

    @When("manager asks for reports")
    public void managerAsksForReports() {
        report = ManagerAPI.getReport();
        reportForMerchant = false;
    }

    @Then("a payment of amount {int} kr from the customer to the merchant is found")
    public void aPaymentOfAmountKrFromTheCustomerToTheMerchantIsFound(Integer amount) {
        assertNotNull(report);
        assertTrue(report.transactions.size() > 0);
        if (reportForMerchant)
        {
            assertNotNull(report.transactions.stream().filter(t ->
                    t.getAmount().equals(amount) &&
                            t.getCustomerDTUPayId().equals("") && //hidden customer data
                            t.getCustomerBankId().equals("") &&
                            t.getMerchangDTUPayId().equals(merchantDTUPayID) &&
                            t.getMerchantBankId().equals(merchantBankID)
            ).findAny().orElse(null));
        }
        else
        {
            assertNotNull(report.transactions.stream().filter(t ->
                    t.getAmount().equals(amount) &&
                            t.getCustomerDTUPayId().equals(customerDTUPayID) &&
                            t.getCustomerBankId().equals(customerBankID) &&
                            t.getMerchangDTUPayId().equals(merchantDTUPayID) &&
                            t.getMerchantBankId().equals(merchantBankID)
            ).findAny().orElse(null));
        }
    }

    @After
    public void testEnd() {
        BankSOAPService.removeAllAccounts();
        if (customerDTUPayID != null) {
            ManagerAPI.removeAccount(customerDTUPayID);
        }
        if (merchantDTUPayID != null) {
            ManagerAPI.removeAccount(merchantDTUPayID);
        }
    }


    @Then("the payment is not successful")
    public void thePaymentIsNotSuccessful() {
        assertFalse(donePayment);
    }


}
