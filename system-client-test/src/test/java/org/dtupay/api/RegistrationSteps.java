/*
 * Author: Eriks Markevics(s202741)
 */

package org.dtupay.api;

import dtu.ws.fastmoney.User;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.dtupay.data.ResponseType;
import org.dtupay.data.Tokens;
import org.dtupay.exceptions.InvalidTokenNumException;
import org.dtupay.utils.BankSOAPService;
import org.dtupay.utils.ResponseCode;
import org.junit.After;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class RegistrationSteps {


    String customerFirstName = null;
    String customerLastName = null;
    String customerCPR = null;
    String customerBankID = null;
    Boolean customerRegisterSuccessful = false;
    String customerMessage = null;
    Tokens tokens = null;
    String customerDtuPayID = null;

//    @Test
//    @Before
//    public void testBefore() {
//        BankSOAPService.removeAllAccounts();
//    }

    @Given("there is a user with name {string}, lastName {string}, cpr {string} and then register a new bank account with amount {int}")
    public void thereIsACustomerWithNameCarloLastNameMeroniCprAndBankAccID(String firstname, String lastname,
                                                                           String cpr, int amount) {
        customerFirstName = firstname;
        customerLastName = lastname;
        customerCPR = cpr;
        User user = new User();
        user.setCprNumber(cpr);
        user.setLastName(lastname);
        user.setFirstName(firstname);
        customerBankID = BankSOAPService.createAccount(user, BigDecimal.valueOf(amount));

    }

    @When("a registration request is sent")
    public void aRegistrationRequestIsSent() {
        try {
            ResponseType response = CustomerAPI.registerAccount(customerCPR, customerFirstName, customerLastName, customerBankID);
            if (response.code == ResponseCode.REGISTRATION_SUCCESSFUL.getCode()) {
                customerRegisterSuccessful = true;
                customerDtuPayID = response.message;
            } else {
                customerRegisterSuccessful = false;
                customerMessage = response.message;
                System.out.println(customerMessage);
                customerDtuPayID = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            customerRegisterSuccessful = false;
        }
    }

    @Then("the registration is successful")
    public void theRegistrationIsSuccessful() {
        assertTrue(customerRegisterSuccessful);
        tokens = new Tokens();
        tokens.tokens = new ArrayList<>();
    }


    @Then("the registration is failed")
    public void theRegistrationIsFailed() {
        assertFalse(customerRegisterSuccessful);
    }

    @And("now the tokens number is 0")
    public void nowTheTokensNumberIs0() {
        assertEquals(0, tokens.tokens.size());
    }

    @Then("request for {int} new tokens successfully")
    public void requestForNewTokens(int num) throws InvalidTokenNumException {
        int originalLength = tokens.tokens == null ? 0 : tokens.tokens.size();
        tokens = CustomerAPI.requestNewTokens(customerDtuPayID, num);
        assertNotNull(tokens);
        assertEquals(num + originalLength, tokens.tokens.size());
    }

    @Then("request for {int} new tokens failed")
    public void requestForNewTokensFailed(int num) {
//        int originalLength = tokens.tokens == null ? 0 : tokens.tokens.size();
        try {
            tokens = CustomerAPI.requestNewTokens(customerDtuPayID, num);
        } catch (InvalidTokenNumException e) {
            assertTrue(true);
        } catch (Exception e) {
            fail("Unknown Error");
        }
    }

    @After
    public void after() {
        BankSOAPService.removeAllAccounts();
        if (customerDtuPayID != null) {
            ManagerAPI.removeAccount(customerDtuPayID);
        }
    }

}

