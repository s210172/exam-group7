Feature: Payment





#  ======================BEGIN================================
   # Author: Yinggang Mi(s202542)
#  Tests about Payment
  Scenario Outline: Successful Payment
    Given the customer <customerFirstName> <customerLastName> with CPR <customerCPR> has a bank account with balance <customBalance>
    And that the customer is registered with DTU Pay
    And the customer requested for his tokens
    Given a merchant <merchantFirstName> <merchantLastName> with CPR <merchantCPR> has a bank account with balance <merchantBalance>
    And that the merchant is registered with DTU Pay
    When the merchant initiates a payment for <amount> kr by the customer
    Then the payment is successful
    And the balance of the customer at the bank is <cBalance> kr
    And the balance of the merchant at the bank is <mBalance> kr
    Examples:
      | customerFirstName | customerLastName | customerCPR | customBalance | merchantFirstName | merchantLastName | merchantCPR | merchantBalance | amount | cBalance | mBalance |
      | "Fan"             | "group7_314159"  | "32345678"  | 1000          | "Xu2"             | "group7_314159"  | "89654321"  | 1000            | 500    | 500      | 1500     |
      | "S1"              | "group7_314159"  | "43614895"  | 10000000      | "s4"              | "group7_314159"  | "22148597"  | 10000000        | 50000  | 9950000  | 10050000 |
      | "Hahahah"         | "group7_314159"  | "98541236"  | 1             | "kkkkk"           | "group7_314159"  | "55885569"  | 1               | 1      | 0        | 2        |


  Scenario: Failure Payment-Not enough funds
    Given the customer "XCarlo" "group7_314159" with CPR "22042453" has a bank account with balance 1000
    And that the customer is registered with DTU Pay
    And the customer requested for his tokens
    Given a merchant "XEriks" "group7_314159" with CPR "22042454" has a bank account with balance 1000
    And that the merchant is registered with DTU Pay
    When the merchant initiates a payment for 1100 kr by the customer
    Then the payment is not successful
    And the balance of the customer at the bank is 1000 kr
    And the balance of the merchant at the bank is 1000 kr

  Scenario Outline: Successful Payment and reporting from customer
    Given the customer <customerFirstName> <customerLastName> with CPR <customerCPR> has a bank account with balance <customBalance>
    And that the customer is registered with DTU Pay
    And the customer requested for his tokens
    Given a merchant <merchantFirstName> <merchantLastName> with CPR <merchantCPR> has a bank account with balance <merchantBalance>
    And that the merchant is registered with DTU Pay
    When the merchant initiates a payment for <amount> kr by the customer
    Then the payment is successful
    When customer asks for reports
    Then a payment of amount <amount> kr from the customer to the merchant is found
    Examples:
      | customerFirstName | customerLastName | customerCPR | customBalance | merchantFirstName | merchantLastName | merchantCPR | merchantBalance | amount |
      | " Fan"            | "group7_314159"  | "22545678"  | 1000          | "Xu"              | "group7_314159"  | "87654521"  | 1000            | 500    |

  Scenario Outline: Successful Payment and reporting from merchant
    Given the customer <customerFirstName> <customerLastName> with CPR <customerCPR> has a bank account with balance <customBalance>
    And that the customer is registered with DTU Pay
    And the customer requested for his tokens
    Given a merchant <merchantFirstName> <merchantLastName> with CPR <merchantCPR> has a bank account with balance <merchantBalance>
    And that the merchant is registered with DTU Pay
    When the merchant initiates a payment for <amount> kr by the customer
    Then the payment is successful
    When merchant asks for reports
    Then a payment of amount <amount> kr from the customer to the merchant is found
    Examples:
      | customerFirstName | customerLastName | customerCPR | customBalance | merchantFirstName | merchantLastName | merchantCPR | merchantBalance | amount | cBalance | mBalance |
      | "Fan"             | "group7_314159"  | "22512678"  | 1000          | "Pietro"          | "group7_314159"  | "17654521"  | 1000            | 500    | 500      | 1500     |

  Scenario Outline: Successful Payment and reporting from manager
    Given the customer <customerFirstName> <customerLastName> with CPR <customerCPR> has a bank account with balance <customBalance>
    And that the customer is registered with DTU Pay
    And the customer requested for his tokens
    Given a merchant <merchantFirstName> <merchantLastName> with CPR <merchantCPR> has a bank account with balance <merchantBalance>
    And that the merchant is registered with DTU Pay
    When the merchant initiates a payment for <amount> kr by the customer
    Then the payment is successful
    When manager asks for reports
    Then a payment of amount <amount> kr from the customer to the merchant is found
    Examples:
      | customerFirstName | customerLastName | customerCPR | customBalance | merchantFirstName | merchantLastName | merchantCPR | merchantBalance | amount | cBalance | mBalance |
      | "Fan"             | "group7_314159"  | "99512678"  | 1000          | "Luca"            | "group7_314159"  | "99654521"  | 1000            | 500    | 500      | 1500     |

#  ======================END================================


#  ======================BEGIN================================
  # Author: Bogdan Capsa(s210172)
#  Tests about TOKENS

  Scenario: User registration and get 5 tokens
    Given there is a user with name "wenjie2", lastName "group7_314159", cpr "25169873" and then register a new bank account with amount 1000
    When a registration request is sent
    Then the registration is successful
    And now the tokens number is 0
    Then request for 5 new tokens successfully

  Scenario: User try to fetch new token one by one
    Given there is a user with name "wenjie3", lastName "group7_314159", cpr "25169874" and then register a new bank account with amount 1000
    When a registration request is sent
    Then the registration is successful
    And now the tokens number is 0
    Then request for 1 new tokens successfully
    Then request for 1 new tokens successfully
    Then request for 1 new tokens failed


  Scenario: User registration and get more than 5 tokens
    Given there is a user with name "wenjie4", lastName "group7_314159", cpr "25169875" and then register a new bank account with amount 1000
    When a registration request is sent
    Then the registration is successful
    And now the tokens number is 0
    Then request for 1000 new tokens failed


  Scenario: User registration and get -29 tokens
    Given there is a user with name "wenjie5", lastName "group7_314159", cpr "25169876" and then register a new bank account with amount 1000
    When a registration request is sent
    Then the registration is successful
    And now the tokens number is 0
    Then request for -29 new tokens failed

#  ======================END================================




#  ======================BEGIN================================
#  Author:Jiayan Wu(s202534)
#  Tests about Account
  Scenario: Customer registration
    Given there is a user with name "Carlo1", lastName "group7_314159", cpr "11234995" and then register a new bank account with amount 1000
    When a registration request is sent
    Then the registration is successful

  Scenario: Customer registration Failed with two the same customer
    Given there is a user with name "Wenjie", lastName "group7_314159", cpr "10234395" and then register a new bank account with amount 1000
    When a registration request is sent
    Then the registration is successful
    When a registration request is sent
    Then the registration is failed

#  ======================END================================


