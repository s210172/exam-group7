/**
 *
 * @author carlo(s212969)
 */
import org.dtupay.repo.AccountRepo;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

import org.dtupay.data.CreateAccountData;
import org.dtupay.data.User;

public class JUnitTest {


    @org.junit.jupiter.api.Test
    void testAddUser() {
        CreateAccountData createAccountData = new CreateAccountData();
        createAccountData.setBankAccountID(UUID.randomUUID().toString());
        createAccountData.setUser(new User("12345678", "Carlo", "Meroni"));
        String dtupayid = AccountRepo.addAccount(createAccountData);
        assertNotNull(dtupayid);
        String bankId = null;
        try {
            bankId = AccountRepo.getBankIdFromDTUPayId(dtupayid);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
        assertEquals(createAccountData.getBankAccountID(), bankId);
    }

    @org.junit.jupiter.api.Test
    void testRemoveUser() {
        CreateAccountData createAccountData = new CreateAccountData();
        createAccountData.setBankAccountID(UUID.randomUUID().toString());
        createAccountData.setUser(new User("12345678", "Carlo", "Meroni"));
        String dtupayid = AccountRepo.addAccount(createAccountData);
        assertNull(dtupayid);
        int previousNum = AccountRepo.getLength();
        AccountRepo.removeAccount(dtupayid);
        assertEquals(previousNum, AccountRepo.getLength());
    }

    @org.junit.jupiter.api.Test
    void testRemoveFakeUser() {
        String dtupayid = UUID.randomUUID().toString();
        assertFalse(AccountRepo.removeAccount(dtupayid));
    }

}
