/**
 *
 * @author carlo(s212969)
 */
import io.quarkus.runtime.annotations.QuarkusMain;
import io.quarkus.runtime.Quarkus;
import org.dtupay.api.CustomerAPI;
import org.dtupay.api.MerchantAPI;
import org.dtupay.service.CustomerAccountService;
import org.dtupay.service.ManagerAccountService;
import org.dtupay.service.MerchantAccountService;

@QuarkusMain
public class Main {

    public static void main(String... args) {
        System.out.println("Server running!");

        System.out.println("Setting up rabbitmq handlers");
        ManagerAccountService.setup();
        CustomerAccountService.setup();
        MerchantAccountService.setup();

        System.out.println("Running Quarkus!");
        Quarkus.run(args);
    }
}