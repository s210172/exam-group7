/**
 *
 * @author carlo(s212969)
 */

package org.dtupay.api;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;
import org.dtupay.data.CreateAccountData;
import org.dtupay.data.ResponseType;
import org.dtupay.repo.AccountRepo;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

public class CustomerAPI {


    public static void registrationRequest(Event e) {
        System.out.println("EVENT registrationRequest");

        //extract arguments
        String requestId = e.getArgument(0, String.class);
        CreateAccountData createAccountData = e.getArgument(1, CreateAccountData.class);

        String accountId = AccountRepo.addAccount(createAccountData);

        RabbitMqQueue queue = QueueFactory.getQueue();
        if (accountId != null) {
            System.out.println("\tmerchant registered with id " + accountId);
            queue.publish(new Event(EventTopic.REGISTERED.toString(), new Object[]{requestId, new ResponseType(0, accountId)}));
        }
        else {
            System.out.println("\tcouldn't register merchant: account already exists");
            queue.publish(new Event(EventTopic.NOT_REGISTERED.toString(), new Object[]{requestId, new ResponseType(1, "account already exists!")}));
        }
    }
}
