/**
 *
 * @author carlo(s212969)
 */

package org.dtupay.api;

import messaging.Event;
import org.dtupay.data.PaymentRequest;
import org.dtupay.data.TransactionRequest;
import org.dtupay.repo.AccountRepo;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

import java.util.UUID;

public class MerchantAPI {
    public static void paymentRequest(Event e) {
        String requestId = e.getArgument(0, String.class);
        PaymentRequest paymentRequest = e.getArgument(1, PaymentRequest.class);

        System.out.println("EVENT paymentRequest " + requestId);

        try {
            String bankAccountId = AccountRepo.getBankIdFromDTUPayId(paymentRequest.getMerchantDTUPayID());
            TransactionRequest transactionRequest = new TransactionRequest();
            transactionRequest.setAmount(paymentRequest.getAmount());
            transactionRequest.setMerchantBankId(bankAccountId);
            transactionRequest.setMerchangDTUPayId(paymentRequest.getMerchantDTUPayID());
            transactionRequest.setCustomerBankId("");
            transactionRequest.setCustomerDTUPayId("");
            transactionRequest.setDescription(paymentRequest.getDescription());
            transactionRequest.setId(UUID.randomUUID().toString());

            System.out.println("\tmerchant bank id obtained " + requestId);

            Event newEvent = new Event(EventTopic.MERCHANT_BANKID_FOUND.toString(), new Object[]{
                    requestId, transactionRequest
            });
            QueueFactory.getQueue().publish(newEvent);

        } catch (Exception ex) {
            System.out.println("\tcouldn't obtain merchant bank id " + requestId);

            Event newEvent = new Event(EventTopic.PAYMENT_UNSUCCESSFUL.toString(), new Object[]{
                    requestId
            });
            QueueFactory.getQueue().publish(newEvent);
        }
    }

    public static void tokenVerified(Event e) {
        //extract arguments
        String requestId = e.getArgument(0, String.class);
        PaymentRequest paymentRequest = e.getArgument(1, PaymentRequest.class);

        System.out.println("EVENT tokenVerified " + requestId);

        try {
            String customerDTUPayId = paymentRequest.getToken(); //was replaced by token manager
            String bankAccountId = AccountRepo.getBankIdFromDTUPayId(customerDTUPayId);

            TransactionRequest transactionRequest = new TransactionRequest();
            transactionRequest.setAmount(paymentRequest.getAmount());
            transactionRequest.setMerchantBankId("");
            transactionRequest.setMerchangDTUPayId("");
            transactionRequest.setCustomerDTUPayId(customerDTUPayId);
            transactionRequest.setCustomerBankId(bankAccountId);
            transactionRequest.setDescription(paymentRequest.getDescription());
            transactionRequest.setId(UUID.randomUUID().toString());

            System.out.println("\tcustomer bank id obtained " + requestId);
            Event newEvent = new Event(EventTopic.CUSTOMER_BANKID_FOUND.toString(), new Object[]{
                    requestId, transactionRequest
            });
            QueueFactory.getQueue().publish(newEvent);

        } catch (Exception ex) {
            System.out.println("\tcouldn't obtain customer bank id " + requestId);

            Event newEvent = new Event(EventTopic.PAYMENT_UNSUCCESSFUL.toString(), new Object[]{
                    requestId
            });
            QueueFactory.getQueue().publish(newEvent);
        }
    }
}
