/**
 *
 * @author carlo(s212969)
 */

package org.dtupay.api;

import messaging.Event;
import org.dtupay.data.AccountInfo;
import org.dtupay.data.ResponseType;
import org.dtupay.repo.AccountRepo;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;
import org.dtupay.utils.ResponseCode;

public class ManagerAPI {


    public static void removeAccount(Event e) {
        //extract arguments
        String requestId = e.getArgument(0, String.class);
        AccountInfo accountInfo = e.getArgument(1, AccountInfo.class);

        System.out.println("EVENT removeAccountRequest " + requestId);

        boolean accountDeleted = AccountRepo.removeAccount(accountInfo.accountId);
        if (accountDeleted) {
            Event newEvent = new Event(EventTopic.ACCOUNT_REMOVED.toString(), new Object[]{
                    requestId, new ResponseType(ResponseCode.ACCOUNT_REMOVED.getCode(), accountInfo.accountId)
            });
            QueueFactory.getQueue().publish(newEvent);
        } else {
            Event newEvent = new Event(EventTopic.ACCOUNT_NOT_FOUND.toString(), new Object[]{
                    requestId, new ResponseType(ResponseCode.ACCOUNT_NOT_FOUND.getCode(), accountInfo.accountId)
            });
            QueueFactory.getQueue().publish(newEvent);
        }
    }
}
