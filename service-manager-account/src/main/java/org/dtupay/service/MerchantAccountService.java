/**
 *
 * @author carlo(s212969)
 */

package org.dtupay.service;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;
import org.dtupay.api.CustomerAPI;
import org.dtupay.api.ManagerAPI;
import org.dtupay.api.MerchantAPI;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

public class MerchantAccountService {

    public static void setup() {
        RabbitMqQueue queue = QueueFactory.getQueue();
        queue.addHandler(EventTopic.PAYMENT_REQUEST.toString(), MerchantAccountService::handlePaymentRequest);
        queue.addHandler(EventTopic.TOKEN_VERIFIED.toString(), MerchantAccountService::handleTokenVerified);
    }


    public static void handlePaymentRequest(Event e) {
        MerchantAPI.paymentRequest(e);
    }


    public static void handleTokenVerified(Event e) {
        MerchantAPI.tokenVerified(e);
    }


}
