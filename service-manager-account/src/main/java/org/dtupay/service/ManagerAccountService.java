/**
 *
 * @author carlo(s212969)
 */

package org.dtupay.service;

import org.dtupay.api.CustomerAPI;
import org.dtupay.api.ManagerAPI;
import org.dtupay.api.MerchantAPI;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;

public class ManagerAccountService {

    public static void setup() {
        RabbitMqQueue queue = QueueFactory.getQueue();
        queue.addHandler(EventTopic.REMOVE_ACCOUNT_REQUEST.toString(), ManagerAccountService::handleRemoveAccountRequest);
    }

    public static void handleRemoveAccountRequest(Event e) {
        ManagerAPI.removeAccount(e);

    }


}
