/**
 *
 * @author carlo(s212969)
 */

package org.dtupay.service;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;
import org.dtupay.api.CustomerAPI;
import org.dtupay.api.ManagerAPI;
import org.dtupay.api.MerchantAPI;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

public class CustomerAccountService {

    public static void setup() {
        RabbitMqQueue queue = QueueFactory.getQueue();
        queue.addHandler(EventTopic.REGISTRATION_REQUEST.toString(), CustomerAccountService::handleRegistrationRequest);
    }


    public static void handleRegistrationRequest(Event e) {
        CustomerAPI.registrationRequest(e);
    }

}
