/**
 *
 * @author carlo(s212969)
 */
package org.dtupay.aggregates;

import org.dtupay.data.User;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class Account {
	
    private String bankAccountId;
    private String dtuPayId;
    private User user;
    
}
