/**
 *
 * @author carlo(s212969)
 */

package org.dtupay.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.dtupay.aggregates.Account;
import org.dtupay.data.CreateAccountData;

public class AccountRepo {

    private static List<Account> accounts = new ArrayList<>();

    public static void cleanAccount()
    {
        accounts = new ArrayList<>();
    }

    public static String addAccount(CreateAccountData createAccountData) {
        Account account = new Account();
        account.setBankAccountId(createAccountData.getBankAccountID());
        account.setDtuPayId(UUID.randomUUID().toString());
        account.setUser(createAccountData.getUser());

        //check if account already exists
        Account result = accounts.stream()
        						.filter(acc -> acc.getUser().cprNumber.equals(account.getUser().cprNumber))
        						.findAny().orElse(null);
        if (result != null) {
            return null;
        }

        accounts.add(account);
        return account.getDtuPayId();
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public static boolean removeAccount(String dtuPayId) {
        return accounts.removeIf(a -> a.getDtuPayId().equals(dtuPayId));
    }

    public static int getLength()
    {
        return accounts.size();
    }

    public static String getBankIdFromDTUPayId(String dtupayId) throws Exception {
        Account result = accounts.stream().filter(acc -> acc.getDtuPayId().equals(dtupayId)).findAny().orElse(null);
        if (result == null) {
            throw new Exception("Dtupay id not found");
        }
        return result.getBankAccountId();
    }

}
