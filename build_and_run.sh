#!/bin/bash
set -e


# Installation
mvn -f utilities-fm07/pom.xml clean install


mvn -f service-manager-account/pom.xml clean package
mvn -f service-manager-payment/pom.xml clean package
mvn -f service-manager-token/pom.xml clean package
mvn -f service-manager-report/pom.xml clean package
mvn -f service-gateway/pom.xml clean package

# Docker build
docker build service-manager-account --tag service-manager-account-image
docker build service-manager-token --tag service-manager-token-image
docker build service-gateway --tag service-gateway-image
docker build service-manager-payment --tag service-manager-payment-image
docker build service-manager-report --tag service-manager-report-image

# Docker compose
docker-compose up -d

#wait a bit before testing
echo "waiting 10sec to boot everything properly..."
sleep 15

# End-to-end test
cd system-client-test
mvn test
