/**
 * @author Yinggang Mi(s202542)
 */
package org.dtupay.aggregates;

import lombok.Data;

@Data
public class Transaction {
    private Integer amount;
    private String merchantBankId;
    private String merchangDTUPayId;
    private String customerBankId;
    private String customerDTUPayId;
    private String description;
    private String id;
    private Integer time;
}
