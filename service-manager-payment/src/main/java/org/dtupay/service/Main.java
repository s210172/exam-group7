/**
 * @author Yinggang Mi(s202542)
 */
package org.dtupay.service;

import io.quarkus.runtime.annotations.QuarkusMain;
import io.quarkus.runtime.Quarkus;

@QuarkusMain  
public class Main {

    public static void main(String ... args) {
        System.out.println("Server running!");

        System.out.println("Setting up rabbitmq handlers");
        PaymentService.setup();

        System.out.println("Running Quarkus!");
        Quarkus.run(args);
    }
}