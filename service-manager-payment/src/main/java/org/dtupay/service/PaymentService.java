/**
 * @author Yinggang Mi(s202542)
 */
package org.dtupay.service;

import java.math.BigDecimal;
import java.util.HashMap;

import org.dtupay.aggregates.Transaction;
import org.dtupay.data.ResponseType;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import messaging.Event;
import messaging.implementations.RabbitMqQueue;
import org.dtupay.utils.ResponseCode;

public class PaymentService {

	private static HashMap<String, Transaction> requestDataHolder = new HashMap<>();

	public static void setup() {
		RabbitMqQueue queue = QueueFactory.getQueue();
		queue.addHandler(EventTopic.MERCHANT_BANKID_FOUND.toString(), PaymentService::handleMerchantBankIdFound );
		queue.addHandler(EventTopic.CUSTOMER_BANKID_FOUND.toString(), PaymentService::handleCustomerBankIdFound );
	}

	public static void handleMerchantBankIdFound(Event e) {
		//extract arguments
		String requestID = e.getArgument(0, String.class);
		Transaction transaction = e.getArgument(1, Transaction.class);

		System.out.println("EVENT merchantBankIdFound " + requestID);

		//make transaction
		RabbitMqQueue queue = QueueFactory.getQueue();

		//can make transaction
		Transaction t = requestDataHolder.get(requestID);
		if (t != null) {
			//clear hashmap and update transaction data
			t = requestDataHolder.remove(requestID);
			transaction.setCustomerBankId(t.getCustomerBankId());
			transaction.setCustomerDTUPayId(t.getCustomerDTUPayId());

			//perform transaction
			try {
				new BankServiceService().getBankServicePort()
						.transferMoneyFromTo(transaction.getCustomerBankId(), transaction.getMerchantBankId(),
								BigDecimal.valueOf(transaction.getAmount()),
								transaction.getDescription());
				System.out.println("\tmoneyTransfered " + requestID);

				queue.publish(new Event(EventTopic.PAYMENT_SUCCESSFUL.toString(), new Object[]{
						requestID, transaction}
						));
			} catch (BankServiceException_Exception ex) {
				System.out.println("\tfailed to tranfer money " + requestID);
				queue.publish(new Event(EventTopic.PAYMENT_UNSUCCESSFUL.toString(), new Object[]{requestID,
						new ResponseType(ResponseCode.PAYMENT_UNSUCCESSFUL.getCode(), "payment failed")
				}));
			}
		}
		else {
			requestDataHolder.put(requestID, transaction);
		}
	}

	public static void handleCustomerBankIdFound(Event e) {
		//extract arguments
		String requestID = e.getArgument(0, String.class);
		Transaction transaction = e.getArgument(1, Transaction.class);

		System.out.println("EVENT customerBankIdFound " + requestID);

		//make transaction
		RabbitMqQueue queue = QueueFactory.getQueue();

		Transaction t = requestDataHolder.get(requestID);
		if (t != null) {
			//clear hashmap and update transaction data
			t = requestDataHolder.remove(requestID);
			transaction.setMerchantBankId(t.getMerchantBankId());
			transaction.setMerchangDTUPayId(t.getMerchangDTUPayId());

			//perform transaction
			try {
				new BankServiceService().getBankServicePort()
						.transferMoneyFromTo(transaction.getCustomerBankId(), transaction.getMerchantBankId(),
								BigDecimal.valueOf(transaction.getAmount()),
								transaction.getDescription());
				System.out.println("\tmoneyTransfered " + requestID);

				queue.publish(new Event(EventTopic.PAYMENT_SUCCESSFUL.toString(), new Object[]{
						requestID, transaction}
				));
			} catch (BankServiceException_Exception ex) {
				System.out.println("\tfailed to tranfer money " + requestID);
				queue.publish(new Event(EventTopic.PAYMENT_UNSUCCESSFUL.toString(), new Object[]{requestID,
						new ResponseType(ResponseCode.PAYMENT_UNSUCCESSFUL.getCode(), "payment failed")
				}));
			}
		}
		else
		{
			requestDataHolder.put(requestID, transaction);
		}
	}

}
