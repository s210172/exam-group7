/**
 *
 * @author Eriks Markevics(s202741)
 */
package org.dtupay.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.dtupay.data.CreateAccountData;
import org.dtupay.data.PaymentRequest;
import org.dtupay.service.MerchantService;

@Path("/merchants")
public class MerchantAPI {

	@POST
	@Path("/registration")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerMerchant(CreateAccountData createAccountData) {
		return MerchantService.registerMerchant(createAccountData);
	}
	 
	@POST
	@Path("/payment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pay(PaymentRequest paymentData) throws InterruptedException {
		return MerchantService.pay(paymentData);
	}

	@GET
	@Path("/{id}/report")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReport(@PathParam("id") String id) {
		return MerchantService.getReport(id);
	}

}
