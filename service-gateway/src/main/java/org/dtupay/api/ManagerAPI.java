/**
 *
 * @author Eriks Markevics(s202741)
 */
package org.dtupay.api;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.dtupay.service.ManagerService;

@Path("/managers")
public class ManagerAPI {

	@GET
	@Path("/report")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReport() {
		return ManagerService.getReport();
	}

	@GET
	@Path("/remove_account/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRemoveAccount(@PathParam("id") String id) {
		return ManagerService.getRemoveAccount(id);
	}

}

