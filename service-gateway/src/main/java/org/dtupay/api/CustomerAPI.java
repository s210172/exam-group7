/**
 *
 * @author Bogdan Capsa(s210172)
 */
package org.dtupay.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.dtupay.data.CreateAccountData;
import org.dtupay.service.CustomerService;

@Path("/customers")
public class CustomerAPI {

	@POST
	@Path("/registration")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerCustomer(CreateAccountData createAccountData) {
		return CustomerService.registerCustomer(createAccountData);
	}

	@GET
	@Path("/{id}/tokens/{count}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getToken(@PathParam("id") String id, @PathParam("count") Integer count) {
		return CustomerService.getToken(id, count);
	}

	@GET
	@Path("/{id}/report")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReport(@PathParam("id") String id) {
		return CustomerService.getReport(id);

	}

}
