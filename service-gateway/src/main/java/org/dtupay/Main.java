package org.dtupay;

import io.quarkus.runtime.annotations.QuarkusMain;

import io.quarkus.runtime.Quarkus;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.SyncronizedResponse;

@QuarkusMain  
public class Main {

    public static void main(String ... args) {
        System.out.println("Server running!");

        //messaging syncronization system
        //events name that will be used for syncronization
        System.out.println("Setting up rabbitmq handlers!");
        SyncronizedResponse.handleCompleteRequestEvents(new String[] {
                EventTopic.PAYMENT_SUCCESSFUL.toString(),
                EventTopic.PAYMENT_UNSUCCESSFUL.toString(),
                EventTopic.REGISTERED.toString(),
                EventTopic.NOT_REGISTERED.toString(),
                EventTopic.TOKEN_OBTAINED.toString(),
                EventTopic.TOKEN_NOT_OBTAINED.toString(),
                EventTopic.REPORTS_FOUND.toString(),
                EventTopic.REPORTS_NOT_FOUND.toString(),
                EventTopic.ACCOUNT_REMOVED.toString(),
                EventTopic.ACCOUNT_NOT_FOUND.toString(),
        });

        System.out.println("Running Quarkus!");
        Quarkus.run(args);
    }
}