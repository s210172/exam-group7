/**
 *
 * @author Eriks Markevics(s202741)
 */
package org.dtupay.service;

import messaging.Event;
import org.dtupay.data.*;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.ResponseCode;
import org.dtupay.utils.SyncronizedResponse;

import javax.ws.rs.core.Response;

public class MerchantService {

    public static Response registerMerchant(CreateAccountData createAccountData) {
        String requestId = SyncronizedResponse.generateUUID();
        System.out.println("POST /merchant/registration id: " + requestId);

        Event e = new Event(EventTopic.REGISTRATION_REQUEST.toString(), new Object[] {
                requestId, createAccountData
        });
        Event responseEvent = SyncronizedResponse.publishAndWaitForResponse(e);
        ResponseType response = responseEvent.getArgument(1, ResponseType.class);
        System.out.println("<-- accountCreated for request " + requestId);
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    public static Response pay(PaymentRequest paymentData) throws InterruptedException {
        String requestId = SyncronizedResponse.generateUUID();
        System.out.println("POST /merchant/payment id: " + requestId);

        Event e = new Event(EventTopic.PAYMENT_REQUEST.toString(), new Object[] {
                requestId, paymentData
        });
        Event responseEvent = SyncronizedResponse.publishAndWaitForResponse(e);
        if (responseEvent.getType().equals(EventTopic.PAYMENT_SUCCESSFUL.toString())) {
            System.out.println("<-- Payment successful for request " + requestId);
            return Response.status(Response.Status.OK).entity(
                    new ResponseType(ResponseCode.PAYMENT_SUCCESSFUL.getCode(), "Payment successful")
            ).build();
        }
        else {
            System.out.println("<-- Payment unsuccessful for request " + requestId);
            return Response.status(Response.Status.BAD_REQUEST).entity(
                    new ResponseType(ResponseCode.PAYMENT_UNSUCCESSFUL.getCode(), "Payment unsuccessful")
            ).build();
        }
    }

    public static Response getReport(String id) {
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.accountId = id;

        String requestId = SyncronizedResponse.generateUUID();
        System.out.println("POST /merchant/report id: " + requestId);

        Event e = new Event(EventTopic.USER_REPORTS_REQUEST.toString(), new Object[] {
                requestId, accountInfo
        });
        Event responseEvent = SyncronizedResponse.publishAndWaitForResponse(e);
        if (responseEvent.getType().equals(EventTopic.REPORTS_FOUND.toString())) {
            System.out.println("<-- reportsFound for request " + requestId);
            return Response.status(Response.Status.OK).entity(responseEvent.getArgument(1, Report.class)).build();
        }
        else {
            System.out.println("<-- reportsNotFound for request " + requestId);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
