/**
 *
 * @author Bogdan Capsa(s210172)
 */
package org.dtupay.service;

import messaging.Event;
import org.dtupay.data.*;
import org.dtupay.exceptions.InvalidTokenNumException;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.ResponseCode;
import org.dtupay.utils.SyncronizedResponse;

import javax.ws.rs.core.Response;

public class CustomerService {

    public static Response registerCustomer(CreateAccountData createAccountData) {
        String requestId = SyncronizedResponse.generateUUID();
        System.out.println("POST /customer/registration id: " + requestId);

        Event e = new Event(EventTopic.REGISTRATION_REQUEST.toString(), new Object[]{
                requestId, createAccountData
        });
        Event responseEvent = SyncronizedResponse.publishAndWaitForResponse(e);
        ResponseType response = responseEvent.getArgument(1, ResponseType.class);
        System.out.println("<-- accountCreated for request " + requestId);
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    public static Response getToken(String id, Integer count) {
        RequestTokens requestTokens = new RequestTokens();
        requestTokens.setCustomerDTUPayID(id);
        try {
            requestTokens.setRequestTokenNum(count);
        } catch (InvalidTokenNumException e) {
            ResponseType response = new ResponseType(ResponseCode.TOKEN_ERROR_ILLEGAL_NUMBER_REQUESTED.getCode(), "invalid tokens");
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }

        String requestId = SyncronizedResponse.generateUUID();
        System.out.println("POST /customer/token id: " + requestId);

        Event e = new Event(EventTopic.TOKEN_REQUEST.toString(), new Object[]{
                requestId, //this will be the ID of this request
                requestTokens
        });

        Event responseEvent = SyncronizedResponse.publishAndWaitForResponse(e);
        if (responseEvent.getType().equals(EventTopic.TOKEN_OBTAINED.toString())) {
            Tokens tokens = responseEvent.getArgument(1, Tokens.class);
            System.out.println("<-- tokenObtained for request " + requestId);
            return Response.status(Response.Status.OK).entity(tokens).build();
        } else {
            ResponseType response = responseEvent.getArgument(1, ResponseType.class);
            System.out.println("<-- tokenNotObtained for request " + requestId);
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();

        }
    }

    public static Response getReport(String id) {
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.accountId = id;

        String requestId = SyncronizedResponse.generateUUID();
        System.out.println("POST /customer/report id: " + requestId);

        Event e = new Event(EventTopic.USER_REPORTS_REQUEST.toString(), new Object[]{
                requestId, accountInfo
        });
        Event responseEvent = SyncronizedResponse.publishAndWaitForResponse(e);
        if (responseEvent.getType().equals(EventTopic.REPORTS_FOUND.toString())) {
            System.out.println("<-- reportsFound for request " + requestId);
            return Response.status(Response.Status.OK).entity(responseEvent.getArgument(1, Report.class)).build();
        } else {
            System.out.println("<-- reportsNotFound for request " + requestId);
            return Response.status(Response.Status.NOT_FOUND).build();
        }


    }
}
