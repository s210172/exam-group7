/**
 *
 * @author Eriks Markevics(s202741)
 */
package org.dtupay.service;

import messaging.Event;
import org.dtupay.data.AccountInfo;
import org.dtupay.data.Report;
import org.dtupay.data.ResponseType;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.SyncronizedResponse;

import javax.ws.rs.core.Response;

public class ManagerService {

    public static Response getReport() {
        String requestId = SyncronizedResponse.generateUUID();
        System.out.println("POST /manager/report id: " + requestId);

        Event e = new Event(EventTopic.ALL_REPORTS_REQUEST.toString(), new Object[] {
                requestId
        });
        Event responseEvent = SyncronizedResponse.publishAndWaitForResponse(e);
        if (responseEvent.getType().equals(EventTopic.REPORTS_FOUND.toString())) {
            System.out.println("<-- reportsFound for request " + requestId);
            return Response.status(Response.Status.OK).entity(responseEvent.getArgument(1, Report.class)).build();
        }
        else {
            System.out.println("<-- reportsFound for request " + requestId);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    public static Response getRemoveAccount(String id) {
        AccountInfo accountInfo = new AccountInfo();
        accountInfo.accountId = id;

        String requestId = SyncronizedResponse.generateUUID();
        System.out.println("POST /manager/remove_account id: " + requestId);

        Event e = new Event(EventTopic.REMOVE_ACCOUNT_REQUEST.toString(), new Object[] {
                requestId, accountInfo
        });
        Event responseEvent = SyncronizedResponse.publishAndWaitForResponse(e);
        if (responseEvent.getType().equals(EventTopic.ACCOUNT_REMOVED.toString())) {
            System.out.println("<-- account removed " + requestId);
            return Response.status(Response.Status.OK).entity(responseEvent.getArgument(1, ResponseType.class)).build();
        }
        else {
            System.out.println("<-- account not found " + requestId);
            return Response.status(Response.Status.NOT_FOUND).entity(responseEvent.getArgument(1, ResponseType.class)).build();
        }
    }

}
