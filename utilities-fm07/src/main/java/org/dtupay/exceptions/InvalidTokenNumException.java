package org.dtupay.exceptions;

public class InvalidTokenNumException extends Exception {
    public InvalidTokenNumException(String msg)
    {
        super(msg);
    }

    public InvalidTokenNumException(int num)
    {
        super("invalid input num:"+num);
    }
}
