package org.dtupay.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentRequest {
    private Integer amount;
    private String token;
    private String merchantDTUPayID;
    private String description;
}
