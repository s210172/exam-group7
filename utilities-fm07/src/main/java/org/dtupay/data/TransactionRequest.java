package org.dtupay.data;

import lombok.Data;

@Data
public class TransactionRequest {
    private Integer amount;
    private String merchantBankId;
    private String merchangDTUPayId;
    private String customerBankId;
    private String customerDTUPayId;
    private String description;
    private String id;
    private Integer time;
}
