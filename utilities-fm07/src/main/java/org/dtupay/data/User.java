package org.dtupay.data;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class User {
   public String cprNumber;
   public String firstName;
   public String lastName;

}
