package org.dtupay.data;

import org.dtupay.exceptions.InvalidTokenNumException;

import lombok.Getter;
import lombok.Setter;

public class RequestTokens {

	@Getter @Setter
	private String customerDTUPayID;
	@Setter @Getter
	private Integer requestTokenNum;

	public void setRequestTokenNum(Integer requestTokenNum) throws InvalidTokenNumException {
		if (requestTokenNum < 1 || requestTokenNum > 5) throw new InvalidTokenNumException(requestTokenNum);
		else this.requestTokenNum = requestTokenNum;
	}

}
