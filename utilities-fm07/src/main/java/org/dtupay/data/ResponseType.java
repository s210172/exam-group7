package org.dtupay.data;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ResponseType {

    public Integer code;
	public String message;

}