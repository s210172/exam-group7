/**
 *
 * @author Eriks Markevics(s202741)
 */
package org.dtupay.utils;

public enum ResponseCode {
    PAYMENT_SUCCESSFUL(0),
    PAYMENT_UNSUCCESSFUL(1),
    REGISTRATION_SUCCESSFUL(0),
    REGISTRATION_FAILED_USER_ALREAD_EXISTS(1),
    REPORTS_FOUND(0),
    REPORTS_NOT_FOUND(1),
    TOKEN_OBTAINED(0),
    TOKEN_ERROR_USER_NOT_FOUND(1),
    TOKEN_ERROR_ILLEGAL_NUMBER_REQUESTED(2),
    TOKEN_ERROR_HAVE_ENOUGH_TOKENS(3),
    ACCOUNT_REMOVED(0),
    ACCOUNT_NOT_FOUND(1);

    private final Integer code;

    ResponseCode(final Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return this.code;
    }
}
