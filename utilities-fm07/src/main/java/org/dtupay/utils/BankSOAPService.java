/**
 * @author Eriks Markevics(s202741)
 */
package org.dtupay.utils;

import dtu.ws.fastmoney.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BankSOAPService {

    public static BankService bank = new BankServiceService().getBankServicePort();
    private static List<String> usersIds = new ArrayList<String>();

    public static String createAccount(User user, BigDecimal balance) {
        try {
            String bankId = bank.createAccountWithBalance(user, balance);
            System.out.println("Bank account created for " + user.getFirstName() + " with id " + bankId);
            usersIds.add(bankId);
            return bankId;
        } catch (BankServiceException_Exception e) {
            System.out.println("Could not create bank account");
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public static void removeAllAccounts() {
        System.out.println("Clear bank accounts!");

        for (String id : usersIds) {
            try {
                bank.retireAccount(id);
                System.out.println("Removing account " + id);
            } catch (BankServiceException_Exception e) {
                e.printStackTrace();
            }
        }
        usersIds.clear();

        //cleaning trick
        //retire all accounts with special last name
        //special lastname id
        String specialLastName = "group7_314159";
        List<AccountInfo> ais = bank.getAccounts();
        for (AccountInfo ai : ais) {
            if (ai.getUser().getLastName().equals(specialLastName)) {
                try {
                    bank.retireAccount(ai.getAccountId());
                    System.out.println("Removing account " + ai.getAccountId() + " with special lastName");
                } catch (BankServiceException_Exception e) {
                }
            }
        }

    }
}
