/**
 *
 * @author Eriks Markevics(s202741)
 */
package org.dtupay.utils;

import messaging.Event;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SyncronizedResponse {

    private static HashMap<String, CompletableFuture<Event>> eventMap = new HashMap<>();

    public static void handleCompleteRequestEvents(String[] responseEventNames) {
        for (String name : responseEventNames) {
            QueueFactory.getQueue().addHandler(name, SyncronizedResponse::completeRequestEvent);
        }
    }

    public static void handleCompleteRequestEvent(String responseEventName) {
        QueueFactory.getQueue().addHandler(responseEventName, SyncronizedResponse::completeRequestEvent);
    }

    //used to generate requestIDs
    public static String generateUUID() {
        return UUID.randomUUID().toString();
    }

    //publish the event in the queue and waits that an event with
    //same requestID triggers the handleCompleteRequestEvent to
    //continue, and returns that event
    public static Event publishAndWaitForResponse(Event event) {
        String requestID = event.getArgument(0, String.class);

        //special event must wait for response
        CompletableFuture<Event> futureEvent = new CompletableFuture<>();
        eventMap.put(requestID, futureEvent);
        QueueFactory.getQueue().publish(event);
        //wait for response
        /*
        try {
            return futureEvent.get(10000, TimeUnit.MILLISECONDS);
        } catch (TimeoutException | InterruptedException | ExecutionException e) {
            System.out.println("Timeout waiting for response with id: " + requestID);
            return new Event();
        }
         */
        return futureEvent.join();
    }

    private static void completeRequestEvent(Event e) {
        String requestID = e.getArgument(0, String.class);
        CompletableFuture<Event> futureEvent = eventMap.get(requestID);
        if (futureEvent == null){
            System.out.println("No future event found in queue");
            System.out.println("\tEvent type: " + e.getType().toString());
            System.out.println("\tRequest id: " + requestID);
        }
        else {
            System.out.println("Response found!");
            System.out.println("\tEvent removed: " + requestID);
            System.out.println("\tEvent type: " + e.getType().toString());
            eventMap.remove(requestID);
            futureEvent.complete(e);
        }
    }


}
