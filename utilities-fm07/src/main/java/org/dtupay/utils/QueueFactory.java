/**
 *
 * @author Bogdan Capsa(s210172)
 */
package org.dtupay.utils;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;

public class QueueFactory {
    private static RabbitMqQueue queue = null;

    public static RabbitMqQueue getQueue() {
        if (queue == null) {
            queue = new RabbitMqQueue();
        }
        return queue;
    }

    //modified
    //[wenjie]
    // set hostname
    public static RabbitMqQueue getQueue(String host) {
        if (queue == null) {
            queue = new RabbitMqQueue(host);
        }
        return queue;
    }

}
