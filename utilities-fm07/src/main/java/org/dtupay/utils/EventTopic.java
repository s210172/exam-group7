/**
 *
 * @author Bogdan Capsa(s210172)
 */
package org.dtupay.utils;

public enum EventTopic {
    PAYMENT_REQUEST("paymentRequest"),
    PAYMENT_SUCCESSFUL("paymentSuccessful"),
    PAYMENT_UNSUCCESSFUL("paymentUnsuccessful"),

    MERCHANT_BANKID_FOUND("merchantBankIdFound"),
    CUSTOMER_BANKID_FOUND("customerBankIdFound"),

    REGISTRATION_REQUEST("registrationRequest"),
    REGISTERED("registered"),
    NOT_REGISTERED("notRegistered"),

    ALL_REPORTS_REQUEST("allReportsRequest"),
    USER_REPORTS_REQUEST("userReportsRequest"),
    REPORTS_FOUND("reportsFound"),
    REPORTS_NOT_FOUND("reportsNotFound"),

    REMOVE_ACCOUNT_REQUEST("removeAccountRequest"),
    ACCOUNT_REMOVED("accountRemoved"),
    ACCOUNT_NOT_FOUND("accountNotFound"),

    //One customer requests for a token, the service-gateway publish "tokenRequest" topic
    TOKEN_REQUEST("tokensRequest"),
    //Check if the customer still have usable token. If he has ,refuse. Otherwise return tokens.
    TOKEN_OBTAINED("tokenObtained"),
    TOKEN_NOT_OBTAINED("tokenNotObtained"),

    //if it is valid, replace the token with mid and publish
    TOKEN_VERIFIED("tokenVerified");


    private final String text;

    EventTopic(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}
