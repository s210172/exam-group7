/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:TokenManagerTestSteps.java
 *    Date:2022 1 21
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 21
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager;

import io.cucumber.java.After;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import messaging.Event;
import org.dtupay.data.PaymentRequest;
import org.dtupay.data.RequestTokens;
import org.dtupay.data.ResponseType;
import org.dtupay.exceptions.InvalidTokenNumException;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.SyncronizedResponse;
import org.tokenmanager.exception.CustomerNotFoundException;
import org.tokenmanager.repo.AccountStorageImplLocal;
import org.tokenmanager.service.CustomerTokenService;
import org.tokenmanager.service.MerchantTokenService;

import java.util.UUID;

import static org.junit.Assert.*;

public class TokenManagerTestSteps {
    AccountStorageImplLocal accountStorageImplLocal;
    Event currentEvent;
    String currentRequestID;
    String currentAccountID;
    int previousRecordListSize = 0;
    int previousTokenSize = 0;


    @After
    public void testEnd() {

    }

    @Given("a newTokenManager")
    public void aNewTokenManager() {
        accountStorageImplLocal = AccountStorageImplLocal.getInstance();
        previousRecordListSize = 0;
        previousTokenSize = 0;
    }

    @And("receive an UserRegisterEvent")
    public void receiveAUserRegisterTopic() {
        currentAccountID = UUID.randomUUID().toString();
        currentRequestID = UUID.randomUUID().toString();
        System.out.println("currentAccountID:" + currentAccountID);
        System.out.println("currentRequestID:" + currentRequestID);
        currentEvent = new Event(EventTopic.REGISTERED.toString(), new Object[]{currentRequestID, new ResponseType(0, currentAccountID)});
    }

    @Then("process the UserRegisterEvent")
    public void processTheUserRegisterEvent() {
        previousRecordListSize = accountStorageImplLocal.getAccountRecordLength();
        System.out.println("current record size " + accountStorageImplLocal.getAccountRecordLength());
        CustomerTokenService.handleAccountRegistered(currentEvent);
        System.out.println("after add, now record size " + accountStorageImplLocal.getAccountRecordLength());
    }

    @Then("a new record is stored in manager")
    public void aNewRecordIsStoredInManager() {
        try {
            assertNotNull(accountStorageImplLocal.findCustomerInCid(currentAccountID));
        } catch (CustomerNotFoundException e) {
            fail("Customer not registered!");
        }
    }

    @Then("User register failed for account exists")
    public void userRegisterFailedForAccountExists() {
        if (previousRecordListSize != accountStorageImplLocal.getAccountRecordLength())
            fail("A exists account registered again!");

    }


    @And("receive TokenRegisterTopic with number {int}")
    public void receiveTokenRegisterTopicWithNumberNum(int num) {
        RequestTokens requestTokens = new RequestTokens();
        try {
            requestTokens.setRequestTokenNum(num);
            requestTokens.setCustomerDTUPayID(currentAccountID);
        } catch (InvalidTokenNumException e) {
            fail("invalid token number");
        }
        currentEvent = new Event(EventTopic.TOKEN_REQUEST.toString(), new Object[]{
                SyncronizedResponse.generateUUID(), //this will be the ID of this request
                requestTokens
        });
        try {
            previousTokenSize = accountStorageImplLocal.getCustomerTokenNum(currentAccountID);
        } catch (CustomerNotFoundException e) {
            fail("customer not registered");
        }
        CustomerTokenService.handleTokensRequest(currentEvent);

    }


    @Then("there are {int} new tokens generated")
    public void numNewTokensGenerated(int num) {
        try {
            assertEquals(num, accountStorageImplLocal.getCustomerTokenNum(currentAccountID) - previousTokenSize);
        } catch (CustomerNotFoundException e) {
            fail("customer not registered");
        }
    }


    @And("receive TokenRegisterTopic with invalid number {int} and found error")
    public void receiveTokenRegisterTopicWithInvalidNumberNum(int num) {
        RequestTokens requestTokens = new RequestTokens();
        try {
            requestTokens.setRequestTokenNum(num);
            requestTokens.setCustomerDTUPayID(currentAccountID);
            fail("invalid token number not tested");
        } catch (InvalidTokenNumException e) {
            assertTrue(true);
        }
    }

    @And("receive an PaymentRequest with correct token")
    public void receiveAnPaymentRequestWithCorrectToken() {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setToken( accountStorageImplLocal.getTokens(currentAccountID).tokens.remove(0));
        currentEvent = new Event(EventTopic.PAYMENT_REQUEST.toString(), new Object[]{
                currentRequestID, //this will be the ID of this request
                paymentRequest
        });
    }


    @Then("the veriftying is done successfully")
    public void theVeriftyingIsDoneSuccessfully() {
        int preivousTokenUseNum = accountStorageImplLocal.getAllTokenUsage().size();
        MerchantTokenService.handlePaymentRequest(currentEvent);
        assertEquals(1, accountStorageImplLocal.getAllTokenUsage().size() - preivousTokenUseNum);
    }

    @And("receive an PaymentRequest with token {string}")
    public void receiveAnPaymentRequestWithToken(String arg0) {
        PaymentRequest paymentRequest = new PaymentRequest();
        paymentRequest.setToken(arg0);
        currentEvent = new Event(EventTopic.PAYMENT_REQUEST.toString(), new Object[]{
                currentRequestID, //this will be the ID of this request
                paymentRequest
        });
    }

    @Then("the token is fake")
    public void theTokenIsFake() {
        int preivousTokenUseNum = accountStorageImplLocal.getAllTokenUsage().size();
        MerchantTokenService.handlePaymentRequest(currentEvent);
        assertEquals(0, accountStorageImplLocal.getAllTokenUsage().size() - preivousTokenUseNum);
    }
}
