/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:TokenManagerLog.java
 *    Date:2022 1 18
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 18
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.utils;



public class TokenManagerLog {

    private  final static String SERVICE_NAME = "service-manager-token";

    public static void tokenManagerLogInfo(String msg)
    {
        System.out.println("logInfo:["+SERVICE_NAME+"]"+":"+msg);
    }

    public static void tokenManagerLogError(String msg)
    {
        System.err.println("logInfo:["+SERVICE_NAME+"]"+":"+msg);
    }

}
