/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:ManagerAPI.java
 *    Date:2022 1 21
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 21
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.api;

import messaging.Event;
import org.dtupay.data.ResponseType;
import org.dtupay.utils.QueueFactory;
import org.tokenmanager.repo.AccountStorageImplLocal;

public class ManagerAPI {



    public static void publishQueue(String topic, Object[] objects) throws java.lang.Error {
        QueueFactory.getQueue().publish(new Event(topic, objects));
    }

    public static void managerRemoveAccount(Event e) {
        String requestId = e.getArgument(0, String.class);
        ResponseType response = e.getArgument(1, ResponseType.class);
        String customerId = response.message;

        System.out.println("EVENT accountRemoved " + requestId);

        AccountStorageImplLocal accountManager = AccountStorageImplLocal.getInstance();
        accountManager.removeCustomer(customerId);
    }



}
