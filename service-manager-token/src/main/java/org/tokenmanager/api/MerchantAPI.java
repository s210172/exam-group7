/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:MerchantAPI.java
 *    Date:2022 1 21
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 21
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.api;

import messaging.Event;
import org.dtupay.data.PaymentRequest;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;
import org.tokenmanager.aggregates.Customer;
import org.tokenmanager.exception.TokenNotFoundException;
import org.tokenmanager.repo.AccountStorageImplLocal;
import org.tokenmanager.utils.TokenManagerLog;

public class MerchantAPI {


    public static void publishQueue(String topic, Object[] objects) throws java.lang.Error {
        QueueFactory.getQueue().publish(new Event(topic, objects));
    }

    public static void paymentRequest(Event e) {
        String requestId = e.getArgument(0, String.class);
        PaymentRequest paymentRequest = e.getArgument(1, PaymentRequest.class);

        System.out.println("EVENT paymentRequest " + requestId);

        TokenManagerLog.tokenManagerLogInfo("event type:" + e.getType());
        AccountStorageImplLocal accountManager = AccountStorageImplLocal.getInstance();
        handlePaymentRequestException(accountManager, paymentRequest, requestId);
    }


    static void handlePaymentRequestException(AccountStorageImplLocal accountManager, PaymentRequest paymentRequest, String requestId) {
        try {
            Customer customer = accountManager.findCustomerInToken(paymentRequest.getToken());
            paymentRequest = new PaymentRequest(paymentRequest.getAmount(), customer.getDtuPayAccountId(), paymentRequest.getMerchantDTUPayID(), paymentRequest.getDescription());
            // publish the topic to queue -> receiver: service-payment
            accountManager.addTokenUsage(new String[]{paymentRequest.getToken(), requestId});
            TokenManagerLog.tokenManagerLogInfo("Token verified and obtained customerDtuPayID: " + customer.getDtuPayAccountId());
            publishQueue(EventTopic.TOKEN_VERIFIED.toString(), new Object[]{requestId, paymentRequest});
        } catch (TokenNotFoundException ex) {
            // publish the topic -> receiver: service-gateway
            TokenManagerLog.tokenManagerLogInfo("couldn't verify token!");
            try {
                publishQueue(EventTopic.PAYMENT_UNSUCCESSFUL.toString(), new Object[]{requestId, paymentRequest});
            } catch (java.lang.Error exc) {
                System.out.println("ignore java.lang.Error of localhost not found warning in test");
            }
        } catch (java.lang.Error error) {
            System.out.println("ignore java.lang.Error of localhost not found warning in test");

        }
    }

}
