/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:CustomerAPI.java
 *    Date:2022 1 21
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 21
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.api;

import messaging.Event;
import org.dtupay.data.RequestTokens;
import org.dtupay.data.ResponseType;
import org.dtupay.data.Tokens;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;
import org.dtupay.utils.ResponseCode;
import org.tokenmanager.aggregates.Customer;
import org.tokenmanager.exception.CustomerExistsException;
import org.tokenmanager.exception.CustomerNotFoundException;
import org.tokenmanager.exception.HaveEnoughTokensException;
import org.tokenmanager.exception.IllegalTokenRequestNumberException;
import org.tokenmanager.repo.AccountStorageImplLocal;
import org.tokenmanager.utils.TokenManagerLog;

public class CustomerAPI {


    public static void publishQueue(String topic, Object[] objects) throws java.lang.Error {
        QueueFactory.getQueue().publish(new Event(topic, objects));
    }

    public static void newAccountRegistered(Event e) {
        String requestId = e.getArgument(0, String.class);
        ResponseType response = e.getArgument(1, ResponseType.class);

        System.out.println("EVENT accountRegistered " + requestId);

        TokenManagerLog.tokenManagerLogInfo("event type:" + e.getType());
        AccountStorageImplLocal accountManager = AccountStorageImplLocal.getInstance();
        try {
            TokenManagerLog.tokenManagerLogInfo("add new account:" + response.message);
            accountManager.addCustomer(new Customer(response.message));
        } catch (CustomerExistsException ex) {
            // this exception should be handled before
            TokenManagerLog.tokenManagerLogError("event type:" + e.toString() + "\n" + ex.getMessage());
        }
    }

    public static void tokenRequest(Event e) {
        String requestId = e.getArgument(0, String.class);
        RequestTokens requestTokens = e.getArgument(1, RequestTokens.class);

        System.out.println("EVENT tokensRequest " + requestId);

        TokenManagerLog.tokenManagerLogInfo("event type:" + e.getType());
        AccountStorageImplLocal accountManager = AccountStorageImplLocal.getInstance();
        TokenManagerLog.tokenManagerLogInfo("customerDTUPayID:" + requestTokens.getCustomerDTUPayID());
        Customer customer = null;

        // The error here of java.lang.Error is for that the host of rabbitmq is not intialized.
        // So we ingore this error during test
        String warningMessage = "ignore java.lang.Error of localhost not found warning in test";
        try {
            customer = accountManager.findCustomerInCid(requestTokens.getCustomerDTUPayID());
            accountManager.generateNewTokens(requestTokens.getCustomerDTUPayID(), requestTokens.getRequestTokenNum());

            Tokens tokens = accountManager.getTokens(requestTokens.getCustomerDTUPayID());
            publishQueue(EventTopic.TOKEN_OBTAINED.toString(), new Object[]{requestId, tokens});
            TokenManagerLog.tokenManagerLogInfo(requestTokens.getCustomerDTUPayID() + " get the new tokens:" + tokens.tokens.size());
        } catch (CustomerNotFoundException ex) {
            TokenManagerLog.tokenManagerLogError(ex.getMessage());
            try {
                publishQueue(EventTopic.TOKEN_NOT_OBTAINED.toString(),
                        new Object[]{requestId,
                                new ResponseType(ResponseCode.TOKEN_ERROR_USER_NOT_FOUND.getCode(), ex.getMessage())
                        });
            } catch (java.lang.Error exc) {
                System.out.println(warningMessage);
            }
        } catch (IllegalTokenRequestNumberException ex) {
            TokenManagerLog.tokenManagerLogError(ex.getMessage());
            try {
                publishQueue(EventTopic.TOKEN_NOT_OBTAINED.toString(),
                        new Object[]{requestId,
                                new ResponseType(ResponseCode.TOKEN_ERROR_ILLEGAL_NUMBER_REQUESTED.getCode(), ex.getMessage())});
            } catch (java.lang.Error exc) {
                System.out.println(warningMessage);
            }
        } catch (HaveEnoughTokensException haveEnoughTokensException) {
            TokenManagerLog.tokenManagerLogError(haveEnoughTokensException.getMessage());
            try {
                publishQueue(EventTopic.TOKEN_NOT_OBTAINED.toString(),
                        new Object[]{requestId,
                                new ResponseType(ResponseCode.TOKEN_ERROR_HAVE_ENOUGH_TOKENS.getCode(), haveEnoughTokensException.getMessage())}
                );
            } catch (java.lang.Error ex) {
                System.out.println(warningMessage);
            }
        } catch (java.lang.Error error) {
            System.out.println(warningMessage);

        }

    }

}
