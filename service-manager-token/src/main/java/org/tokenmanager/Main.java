/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:Main.java
 *    Date:2022 1 21
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 21
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager;

import io.quarkus.runtime.annotations.QuarkusMain;
import io.quarkus.runtime.Quarkus;
import org.tokenmanager.service.CustomerTokenService;
import org.tokenmanager.service.ManagerTokenService;
import org.tokenmanager.service.MerchantTokenService;

@QuarkusMain
public class Main {

    public static void main(String... args) {
        System.out.println("Running Quarkus!");
        Main.setup();
        Quarkus.run(args);
    }

    private static void setup() {
        System.out.println("Setting up rabbitmq handlers");
        MerchantTokenService.setup();
        CustomerTokenService.setup();
        ManagerTokenService.setup();
    }
}