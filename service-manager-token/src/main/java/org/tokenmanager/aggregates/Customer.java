
/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:Customer.java
 *    Date:2022 1 20
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 20
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.aggregates;

import java.util.ArrayList;
import java.util.Objects;

import lombok.Data;

@Data
public class Customer {
    private String dtuPayAccountId;
    private ArrayList<Token> tokens;

    public static final int MAX_TOKENS = 6;

    public Customer(String accountId) {
        this.dtuPayAccountId = accountId;
        this.tokens = new ArrayList<Token>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(dtuPayAccountId, customer.dtuPayAccountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dtuPayAccountId);
    }
}
