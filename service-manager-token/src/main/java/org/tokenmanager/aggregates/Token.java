/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:Token.java
 *    Date:2022 1 20
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 20
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.aggregates;

import java.util.UUID;

import lombok.Getter;

public class Token {
	
	@Getter private final String token;

    public Token() {
        this.token = UUID.randomUUID().toString();
    }

}
