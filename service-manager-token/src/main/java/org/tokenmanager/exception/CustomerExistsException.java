/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:CustomerExistsException.java
 *    Date:2022 1 18
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 18
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.exception;

public class CustomerExistsException extends Exception{
    public CustomerExistsException(String accountId) {
        super(accountId);
    }
}
