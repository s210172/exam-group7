/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:HaveEnoughTokensException.java
 *    Date:2022 1 20
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 20
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.exception;

public class HaveEnoughTokensException extends Exception {
    public HaveEnoughTokensException(String s) {
        super(s);
    }
}
