/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:CustomerNotFoundException.java
 *    Date:2022 1 18
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 18
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.exception;

public class CustomerNotFoundException extends Exception{
    public CustomerNotFoundException(String customerID)
    {
        super(customerID);
    }
}
