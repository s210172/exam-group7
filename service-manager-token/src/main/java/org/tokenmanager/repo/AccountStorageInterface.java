/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:AccountStorageInterface.java
 *    Date:2022 1 21
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 21
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.repo;

import org.dtupay.data.Tokens;
import org.tokenmanager.aggregates.Customer;
import org.tokenmanager.exception.*;

import java.util.HashMap;

public interface AccountStorageInterface {
    public void addCustomer(Customer customer) throws CustomerExistsException;

    public void removeCustomer(String customerId);

    public Customer findCustomerInCid(String customerId) throws CustomerNotFoundException;

    public Customer findCustomerInToken(String token) throws TokenNotFoundException;

    public void generateNewTokens(String customerId, int num) throws CustomerNotFoundException, IllegalTokenRequestNumberException, HaveEnoughTokensException;

    public Tokens getTokens(String customerId);

    public String[] getTokenUsage(String token) throws TokenNotFoundException;

    public void addTokenUsage(String[] usageInfo);

    public HashMap<String,String> getAllTokenUsage();

}
