/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:AccountStorageImplLocal.java
 *    Date:2022 1 18
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 18
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.repo;

import org.dtupay.data.Tokens;
import org.tokenmanager.aggregates.Customer;
import org.tokenmanager.aggregates.Token;
import org.tokenmanager.exception.*;
import org.tokenmanager.utils.TokenManagerLog;

import java.util.ArrayList;
import java.util.HashMap;

public class AccountStorageImplLocal implements AccountStorageInterface {
    // Initializaiton
    //Singleton Pattern
    private final ArrayList<Customer> accountRecords;
    private final HashMap<String, String> tokenUsageRecords;
    private static final AccountStorageImplLocal instance = new AccountStorageImplLocal();

    public static AccountStorageImplLocal getInstance() {
        return instance;
    }


    private AccountStorageImplLocal() {
        this.accountRecords = new ArrayList<>();
        this.tokenUsageRecords = new HashMap<>();
    }

    public int getAccountRecordLength() {
        return this.accountRecords.size();
    }

    public int getCustomerTokenNum(String customerId) throws CustomerNotFoundException {
        Object[] customers = (accountRecords.stream().filter(a -> a.getDtuPayAccountId().equals(customerId))).toArray();
        if (customers.length == 0) throw new CustomerNotFoundException("Customer not found when get token num");
        else return ((Customer) customers[0]).getTokens().size();
    }


    public void addCustomer(Customer newCustomer) throws CustomerExistsException {
        if (accountRecords.contains(newCustomer))
            throw new CustomerExistsException(newCustomer.getDtuPayAccountId());
        accountRecords.add(newCustomer);
        System.out.println("add new Customer:" + newCustomer.getDtuPayAccountId());
    }

    public void removeCustomer(String customerId) {
        accountRecords.removeIf(a -> a.getDtuPayAccountId().equals(customerId));
    }

    @Override
    public Customer findCustomerInCid(String customerId) throws CustomerNotFoundException {

        for (Customer customerToCheck : accountRecords) {
            if (customerToCheck.getDtuPayAccountId().equals(customerId)) {
                return customerToCheck;
            }
        }
        throw new CustomerNotFoundException(customerId);
    }

    @Override
    public Customer findCustomerInToken(String token) throws TokenNotFoundException {

        for (Customer customerToCheck : accountRecords) {
            if (checkToken(token, customerToCheck)) {
                return customerToCheck;
            }
        }
        throw new TokenNotFoundException(token);
    }

    public boolean checkToken(String tokenToLookFor, Customer customerToCheck) {
        for (Token tokenToCheck : customerToCheck.getTokens()) {
            if (tokenToCheck.getToken().equals(tokenToLookFor)) {
                // remove token used
                customerToCheck.getTokens().remove(tokenToCheck);
                TokenManagerLog.tokenManagerLogInfo("remove one token");
                return true;
            }
        }
        return false;
    }

    @Override
    public Tokens getTokens(String customerId) {
        Customer customer = accountRecords.stream().filter(c -> c.getDtuPayAccountId().equals(customerId)).findAny().orElse(null);
        Tokens tokens = new Tokens();
        tokens.tokens = new ArrayList<>();
        if (customer != null) {
            for (Token t : customer.getTokens())
                tokens.tokens.add(t.getToken());
        }
        return tokens;
    }

    @Override
    public String[] getTokenUsage(String token) throws TokenNotFoundException {
        try {
            return new String[]{token, tokenUsageRecords.get(token)};
        } catch (Exception e) {
            throw new TokenNotFoundException("token is not used or is fake");
        }
    }

    @Override
    public void addTokenUsage(String[] usageInfo) {
        tokenUsageRecords.put(usageInfo[0], usageInfo[1]);
    }

    @Override
    public HashMap<String, String> getAllTokenUsage() {
        return tokenUsageRecords;
    }

    @Override
    public void generateNewTokens(String customerId, int num) throws CustomerNotFoundException, IllegalTokenRequestNumberException, HaveEnoughTokensException {

        for (Customer customer : accountRecords) {
            if (customer.getDtuPayAccountId().equals(customerId)) {
                if (customer.getTokens().size() != 0 && customer.getTokens().size() != 1)
                    throw new HaveEnoughTokensException(customerId + " still hold enough tokens!");
                else if (customer.getTokens().size() + num > 6 || num <= 0)
                    throw new IllegalTokenRequestNumberException(customerId + " request " + num + "tokens, every one can only hold no more than  6 tokens");
                else {
                    accountRecords.remove(customer);
                    for (int i = 0; i < num; i++) {
                        customer.getTokens().add(new Token());
                    }
                    accountRecords.add(customer);
                }
                return;
            }
        }
        // not found
        throw new CustomerNotFoundException(customerId);
    }


}
