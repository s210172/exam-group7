/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:TokenHandler.java
 *    Date:2022 1 18
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 18
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.service;

import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;
import org.tokenmanager.api.CustomerAPI;
import org.tokenmanager.api.ManagerAPI;
import org.tokenmanager.api.MerchantAPI;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;


public class MerchantTokenService {

    public static void setup() {
        RabbitMqQueue queue = QueueFactory.getQueue();
        //TODO
        queue.addHandler(EventTopic.PAYMENT_REQUEST.toString(), MerchantTokenService::handlePaymentRequest);
    }


    public static void handlePaymentRequest(Event e) {
        MerchantAPI.paymentRequest(e);
    }


}
