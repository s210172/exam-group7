/*
 * Copyright:
 *    Project:service-manager-token
 *    FileName:TokenHandler.java
 *    Date:2022 1 18
 *    Author: fm07-Wenjie Fan(s210310)
 *    Last Modified: 2022 1 18
 *    Modified by: fm07 Wenjie Fan(s210310)
 */

package org.tokenmanager.service;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;
import org.tokenmanager.api.CustomerAPI;
import org.tokenmanager.api.ManagerAPI;
import org.tokenmanager.api.MerchantAPI;


public class CustomerTokenService {

    public static void setup() {
        RabbitMqQueue queue = QueueFactory.getQueue();
        //TODO
        queue.addHandler(EventTopic.TOKEN_REQUEST.toString(), CustomerTokenService::handleTokensRequest);
        queue.addHandler(EventTopic.REGISTERED.toString(), CustomerTokenService::handleAccountRegistered);
    }


    public static void handleAccountRegistered(Event e) {
        CustomerAPI.newAccountRegistered(e);
    }


    public static void handleTokensRequest(Event e) {

        CustomerAPI.tokenRequest(e);

    }


}
