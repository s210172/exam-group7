# Author: Wenjie Fan(s210310)
Feature: TokenManagerTest

#  TOPIC_REGISTERD
# ###########################################################
  Scenario: User register Successfully
    Given a newTokenManager
    And receive an UserRegisterEvent
    Then process the UserRegisterEvent
    Then a new record is stored in manager

  Scenario: User register failed for account exists
    Given a newTokenManager
    And receive an UserRegisterEvent
    Then process the UserRegisterEvent
    Then a new record is stored in manager
    Then process the UserRegisterEvent
    Then User register failed for account exists

# ###########################################################
#  TOPIC_TOKEN_REQUEST
  Scenario Outline: Request Token Successfully
    Given a newTokenManager
    And receive an UserRegisterEvent
    Then process the UserRegisterEvent
    Then a new record is stored in manager
    And receive TokenRegisterTopic with number <num>
    Then there are <num> new tokens generated

    Examples:
      | num |
      | 1   |
      | 2   |
      | 3   |
      | 4   |
      | 5   |

  Scenario Outline: Request Token Failed
    Given a newTokenManager
    And receive an UserRegisterEvent
    Then process the UserRegisterEvent
    Then a new record is stored in manager
    And receive TokenRegisterTopic with invalid number <num> and found error

    Examples:
      | num |
      | 0   |
      | -1  |
      | 6   |
      | 7   |
############################################################
#  TOPIC_Payment_Request
  Scenario: Reply payment request correctly
    Given a newTokenManager
    And receive an UserRegisterEvent
    Then process the UserRegisterEvent
    Then a new record is stored in manager
    And receive TokenRegisterTopic with number 3
    Then there are 3 new tokens generated
    And receive an PaymentRequest with correct token
    Then the veriftying is done successfully


  Scenario: Fake Token
    Given a newTokenManager
    And receive an UserRegisterEvent
    Then process the UserRegisterEvent
    Then a new record is stored in manager
    And receive TokenRegisterTopic with number 3
    Then there are 3 new tokens generated
    And receive an PaymentRequest with token "sadsdasdasdsa"
    Then the token is fake


