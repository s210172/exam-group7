## 1. Introduction

> Group 07 
>
> Members:  Bogdan Capsa(s210172), Carlo Meroni (s212969), Eriks Markevics(s202741), Jiayan Wu(s202534), Wenjie Fan (s210310), Yinggang Mi(s202542)

**Project Structure**:

* **system-test-client**: simulation of customer register, merchant register, the process of requiring token, payment
* **service-gateway**(8081): map all the request to the related message queue.
* **service-manager-account**: managing the accounts of DTU Pay Application
* **service-manager-payment**: the whole process of paying 
* **service-manager-token**: managing customer's tokens
* **rabbit-queue-server**:  official docker image
* **utilities-fm07**: include the part of communicating with `BANK Server` , `Exception`, definition of `Status` and `TOPIC`, and the utilities classes of rabbit message queue. 

## 2. Installation 

### 2.1 Prerequisite

* Maven >= 3 installed and its environment variables/path configured
* Java jdk >= 11
* Docker >= 20.10.7 (Lower is not tested)
* Docker-compose >= 1.25.0 (Lower is not tested)

### 2.2 Unzip and Clean Docker

*  Unzip the zip file

* Run `docker image ls ` to check if the image with tag `rabbitMq` exits. If it exists,  please remove or rename it in advance. Otherwise it will fail for conflict.

* Ensure you are at the root of this project and don't move the file `docker-compose.yaml`.  Run the command :

  ```bash
  ./build_and_run.sh
  ```

* If it passes all the test,  use 

  ```bash
  docker ps -a
  ```

  and you can find 

* The `build_and_run.sh` is shown below. If you fail with tests for `Connection refused localhost:8081`, maybe you need to modify the `sleep` time in our bash script because the test starts before all the docker containers deployed. It depends on your pc/server performance, but for pc 20s waiting is enough. 

  ```bash
  #!/bin/bash
  set -e
  
  
  # Installation
  mvn -f utilities-fm07/pom.xml clean install
  
  
  mvn -f service-manager-account/pom.xml clean package
  mvn -f service-manager-payment/pom.xml clean package
  mvn -f service-manager-token/pom.xml clean package
  mvn -f service-manager-report/pom.xml clean package
  mvn -f service-gateway/pom.xml clean package
  
  # Docker build
  docker build service-manager-account --tag service-manager-account-image
  docker build service-manager-token --tag service-manager-token-image
  docker build service-gateway --tag service-gateway-image
  docker build service-manager-payment --tag service-manager-payment-image
  docker build service-manager-report --tag service-manager-report-image
  
  # Docker compose
  docker-compose up -d
  
  #wait a bit before testing
  echo "waiting 15sec to boot everything properly..."
  sleep 15
  
  # End-to-end test
  cd system-client-test
  mvn test
  ```

  





