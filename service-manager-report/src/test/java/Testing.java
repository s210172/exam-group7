/**
 *
 * @author Jiayan Wu(s202534)
 */

import org.dtupay.repo.ReportingRepo;
import org.dtupay.data.TransactionRequest;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class Testing {



    @Test
    void testAddingTransactionAndGetReports() {
        TransactionRequest transactionRequest = new TransactionRequest();
        transactionRequest.setAmount(1000);
        String customerBankId = UUID.randomUUID().toString();
        String merchantBankId = UUID.randomUUID().toString();
        String customerDtuPayId = UUID.randomUUID().toString();
        String merchantDtuPayId = UUID.randomUUID().toString();
        String transactionId = UUID.randomUUID().toString();
        transactionRequest.setCustomerBankId(customerBankId);
        transactionRequest.setMerchantBankId(merchantBankId);
        transactionRequest.setCustomerDTUPayId(customerDtuPayId);
        transactionRequest.setMerchangDTUPayId(merchantDtuPayId);
        transactionRequest.setDescription("test");
        transactionRequest.setId(transactionId);
        transactionRequest.setTime(1234);
        ReportingRepo.addTransaction(transactionRequest);
        assertEquals(1, ReportingRepo.getAllTransaction().transactions.size());
        assertEquals(1, ReportingRepo.getTransactionById(customerDtuPayId).transactions.size());
        assertEquals(1, ReportingRepo.getTransactionById(merchantDtuPayId).transactions.size());
    }
}
