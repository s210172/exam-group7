/**
 *
 * @author Jiayan Wu(s202534)
 */

package org.dtupay.api;

import messaging.Event;
import org.dtupay.data.TransactionRequest;
import org.dtupay.repo.ReportingRepo;

public class MerchantAPI {
    public static void paymentSuccessful(Event e) {
        System.out.println("EVENT paymentSuccessful");

        //extract arguments
        TransactionRequest transaction = e.getArgument(1, TransactionRequest.class);

        //add new transaction
        ReportingRepo.addTransaction(transaction);
    }


}
