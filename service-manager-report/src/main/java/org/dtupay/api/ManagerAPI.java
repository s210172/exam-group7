/**
 *
 * @author Jiayan Wu(s202534)
 */

package org.dtupay.api;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;
import org.dtupay.aggregates.Report;
import org.dtupay.repo.ReportingRepo;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

public class ManagerAPI {

    public static void allReportRequest(Event e) {
        System.out.println("EVENT allReportsRequest");

        //extract arguments
        String requestId = e.getArgument(0, String.class);

        //get all transactions and send them
        Report report = ReportingRepo.getAllTransaction();
        RabbitMqQueue queue = QueueFactory.getQueue();
        queue.publish(new Event(EventTopic.REPORTS_FOUND.toString(), new Object[]{
                requestId, report}
        ));
    }
}
