/**
 *
 * @author Jiayan Wu(s202534)
 */

package org.dtupay.api;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;
import org.dtupay.aggregates.Report;
import org.dtupay.data.AccountInfo;
import org.dtupay.repo.ReportingRepo;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

public class CustomerAPI {
    public static void userReportRequest(Event e) {
        System.out.println("EVENT userReportsRequest");

        //extract arguments
        String requestId = e.getArgument(0, String.class);
        AccountInfo accountInfo = e.getArgument(1, AccountInfo.class);

        //get user transactions and send them
        Report report = ReportingRepo.getTransactionById(accountInfo.accountId);
        RabbitMqQueue queue = QueueFactory.getQueue();
        //reports found
        if (report.transactions != null && report.transactions.size() > 0) {
            queue.publish(new Event(EventTopic.REPORTS_FOUND.toString(), new Object[]{
                    requestId, report}
            ));
        } else { //reports not found
            queue.publish(new Event(EventTopic.REPORTS_NOT_FOUND.toString(), new Object[]{
                    requestId
            }
            ));
        }
    }
}
