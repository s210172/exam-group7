package org.dtupay;

import io.quarkus.runtime.annotations.QuarkusMain;

import org.dtupay.service.CustomerReportingService;
import org.dtupay.service.ManagerReportingService;
import org.dtupay.service.MerchantReportingService;

import io.quarkus.runtime.Quarkus;

@QuarkusMain
public class Main {

    public static void main(String... args) {
        System.out.println("Server running!");

        System.out.println("Setting up rabbitmq handlers");
        MerchantReportingService.setup();
        CustomerReportingService.setup();
        ManagerReportingService.setup();

        System.out.println("Running Quarkus!");
        Quarkus.run(args);
    }
}