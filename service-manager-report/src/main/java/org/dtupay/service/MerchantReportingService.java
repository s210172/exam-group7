
/**
 *
 * @author Jiayan Wu(s202534)
 */

package org.dtupay.service;

import org.dtupay.api.CustomerAPI;
import org.dtupay.api.ManagerAPI;
import org.dtupay.api.MerchantAPI;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;

public class MerchantReportingService {

    public static void setup() {
        RabbitMqQueue queue = QueueFactory.getQueue();
        queue.addHandler(EventTopic.PAYMENT_SUCCESSFUL.toString(), MerchantReportingService::handlePaymentSuccessful);
    }

    public static void handlePaymentSuccessful(Event e) {
        MerchantAPI.paymentSuccessful(e);
    }



}
