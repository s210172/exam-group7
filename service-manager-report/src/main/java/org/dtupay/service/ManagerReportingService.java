/**
 *
 * @author Jiayan Wu(s202534)
 */


package org.dtupay.service;

import messaging.Event;
import messaging.implementations.RabbitMqQueue;
import org.dtupay.api.CustomerAPI;
import org.dtupay.api.ManagerAPI;
import org.dtupay.api.MerchantAPI;
import org.dtupay.utils.EventTopic;
import org.dtupay.utils.QueueFactory;

public class ManagerReportingService {

    public static void setup() {
        RabbitMqQueue queue = QueueFactory.getQueue();
        queue.addHandler(EventTopic.ALL_REPORTS_REQUEST.toString(), ManagerReportingService::handleAllReportsRequest);

    }


    public static void handleAllReportsRequest(Event e) {
        ManagerAPI.allReportRequest(e);
    }


}
