/**
 *
 * @author Jiayan Wu(s202534)
 */


package org.dtupay.repo;

import java.util.*;
import java.util.stream.Collectors;
import org.dtupay.aggregates.Report;
import org.dtupay.data.TransactionRequest;

public class ReportingRepo {

    private static List<TransactionRequest> transactions = new ArrayList<>();

    public static void addTransaction(TransactionRequest transaction) {
        transactions.add(transaction);
    }

    public static Report getAllTransaction() {
        Report report = new Report();
        report.transactions = transactions;
        return report;
    }

    public static Report getTransactionById(String dtupayId) {
        Report report = new Report();

        //if you are customer show all the data where you are involved
        report.transactions = new ArrayList<>(transactions.stream().filter(t -> (t.getCustomerDTUPayId().equals(dtupayId))).collect(Collectors.toList()));

        //if you are merchant then hide customer data
        List<TransactionRequest> merchantTransactions = new ArrayList<>(transactions.stream().filter(t -> (t.getMerchangDTUPayId().equals(dtupayId))).collect(Collectors.toList()));
        for (Integer i = 0; i < merchantTransactions.size(); i++) {
            TransactionRequest t = merchantTransactions.get(i);
            t.setCustomerBankId(""); //hide customer bank id
            t.setCustomerDTUPayId(""); //hide customer dtu pay id
            merchantTransactions.set(i, t);
        }

        //concatenate transaction where we are customer and where we are merchant
        report.transactions.addAll(merchantTransactions);

        return report;
    }

}
